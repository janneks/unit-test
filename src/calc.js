/**
 * Adds two numbers together.
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
const add = (a, b) => {
  return a + b;
};

/**
 * Subtracts two numbers.
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
const subtract = (a, b) => {
  return a - b;
};
/**
 * multiplies two numbers together.
 * @param {multiplicant} a
 * @param {multiplier} b
 * @returns {number}
 */
const multiply = (multiplicant, multiplier) => {
  return multiplicant * multiplier;
};

/**
 * Divides two numbers.
 * @param {divident} a
 * @param {divisor} b
 * @returns {number}
 * @throws {Error} Cannot divide by zero
 */
const divide = (divident, divisor) => {
  if (divisor === 0) {
    throw new Error('Cannot divide by zero');
  }
  return divident / divisor;
};

export default { add, subtract, multiply, divide };
