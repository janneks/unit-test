import express from 'express';
import calc from './calc.js';

const app = express();
const port = 3000;
const host = 'localhost';

app.get('/', (req, res) => {
  res.status(200).send('Hello World');
});

app.get('/add', (req, res) => {
  const a = parseFloat(req.query.a);
  const b = parseFloat(req.query.b);
  const result = calc.add(a, b);
  res.status(200).send(result.toString());
});

app.listen(port, host, () => {
  console.log(`Server is running at http://${host}:${port}`);
});