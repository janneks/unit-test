import { assert, expect, should } from 'chai';
import { describe } from 'mocha';
import calc from '../src/calc.js';

describe('Calc.js', () => {
  let myVar;
  // before ajetaan kerran ennen tämän describe-lohkon testejä
  before(() => {
    myVar = 1;
  });

  // ajetaan ennen jokaista testiä
  beforeEach(() => {
    myVar++;
  });

  // ajetaan testien jälkeen
  after(() => {
    console.log(`myVar: ${myVar}`);
  });

  it('can add numbers together', () => {
    expect(calc.add(2, 2)).to.equal(4);
    expect(calc.add(2, 1)).to.equal(3);
    expect(calc.add(2, 0.5)).to.equal(2.5);
  });
  it('can subtract numbers', () => {
    assert(calc.subtract(2, 2) === 0);
    assert(calc.subtract(2, 1) === 1);
  });
  it('can multiply numbers', () => {
    should().exist(calc.multiply());
    expect(calc.multiply(2, 0.5)).to.equal(1);
  });
  it('can divide numbers', () => {
    expect(calc.divide(2, 2)).to.equal(1);
    expect(calc.divide(2, 1)).to.equal(2);
    expect(calc.divide(2, 0.5)).to.equal(4);
  });
  // täytyy kääriä nuolifunktion sisään että virheen heitto saadaan kiinni. Jos ei kääritä, niin testi kaatuu ennen kuin virhe heitetään.
  it('cannot divide by zero', () => {
    expect(() => calc.divide(2, 0)).to.throw('Cannot divide by zero');
  });
});

// muista deep jos alat vertailemaan objekteja tai arrayta
